# Docker Pihole
My configuration for setting up pihole in a docker container, set up using a docker compose file.

Traefik is required for web interface access to pihole

## Configuration Directory
CONF_DIR environment variable must be set
****WARNING****
You MUST create the *pihole.log* file in the configuration directory before creating the container, otherwise pihole will throw an error

touch $CONF_DIR/pihole.log is enough

## Env Vars

- CONF_DIR = where the pihole configuration should be saved
- PIHOLE_VHOSTNAME = for traefik, the fqdn to access pihole
- IP = the ip address of the server pihole is running on

## Things to improve
If you're using this in production (why are you using this in production??), determine a stable pihole container tag that works for you, and stick with it. Using latest is *not* best practice.
