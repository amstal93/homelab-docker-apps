# Traefik compose file for reverse proxy

Specify the following values in a .env files

CONF_DIR - where you want to store persistent data
AWS_SECRET_ACCESS_KEY - Secret access key for user accessing AWS
AWS_ACCESS_KEY_ID - access key id for user accessing AWS
AWS_REGION - ideally us-east-1, but route53 is global
AWS_HOSTED_ZONE_ID - the hosted zone where verficiation records are created
GELF_ADDRESS - address of graylog server
GELF_PORT - port of graylog server

You MUST create the acme.json file before bringing up the container. `touch acme.json`
