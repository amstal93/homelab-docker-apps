version: '3.4'
networks:
  app_net: # "backend" network - still has internet access
    driver: bridge
  discovery:
    external: true
services:

  jackett:
    container_name: jackett
    image: linuxserver/jackett:${JACKETT_TAG}
    restart: always
    networks:
      - app_net
      - discovery
    environment:
      - PUID=${PUID} # required for proper nfs access
      - PGID=${PGID} # required for proper nfs access
      - TZ=America/Los_Angeles # timezone
    expose:
      - "9117"
    labels:
      - "traefik.port=9117"
      - "traefik.frontend.rule=Host:${JACKETT_VHOST}"
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${CONF_DIR}/.config/jackett:/config # config files
      - ${DATA_DIR}/downloads/ongoing/torrent-blackhole:/downloads # place where to put .torrent files
    logging:
      driver: gelf
      options:
        gelf-address: "udp://${GELF_ADDRESS}:${GELF_PORT}"
        tag: "jackett"

  sonarr:
    container_name: sonarr
    image: linuxserver/sonarr:${SONARR_TAG}
    restart: always
    depends_on:
      - jackett
    networks:
      - app_net
      - discovery
    environment:
      - PUID=${PUID} # required for proper nfs access
      - PGID=${PGID} # required for proper nfs access
      - TZ=America/Los_Angeles # timezone
    labels:
      - "traefik.port=8989"
      - "traefik.frontend.rule=Host:${SONARR_VHOST}"
    expose:
      - "8989"
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${CONF_DIR}/.config/sonarr:/config # config files
      - ${DATA_DIR}/downloads/series:/tv # tv shows folder
      - ${DATA_DIR}/downloads/ongoing:/downloads # download folder
      - ${DATA_DIR}/downloads/anime:/anime
    logging:
      driver: gelf
      options:
        gelf-address: "udp://${GELF_ADDRESS}:${GELF_PORT}"
        tag: "sonarr"

  radarr:
    container_name: radarr
    image: linuxserver/radarr:${RADARR_TAG}
    restart: always
    depends_on:
      - jackett
    networks:
      - app_net
      - discovery
    environment:
      - PUID=${PUID} # required for proper nfs access
      - PGID=${PGID} # required for proper nfs access
      - TZ=America/Los_Angeles # timezone
    labels:
      - "traefik.port=7878"
      - "traefik.frontend.rule=Host:${RADARR_VHOST}"
    expose:
      - "7878"
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${CONF_DIR}/.config/radarr:/config # config files
      - ${DATA_DIR}/downloads/movies:/movies # movies folder
      - ${DATA_DIR}/downloads/ongoing:/downloads # download folder
    logging:
      driver: gelf
      options:
        gelf-address: "udp://${GELF_ADDRESS}:${GELF_PORT}"
        tag: "radarr"

  plex:
    container_name: plex
    image: linuxserver/plex:${PLEX_TAG}
    restart: always
    depends_on:
      - sonarr
      - radarr
    environment:
      - TZ=America/Los_Angeles # timezone
      - ALLOWED_NETWORKS=192.168.0.0/16
      - PUID=${PUID} # required for proper nfs access
      - PGID=${PGID} # required for proper nfs access
      - PLEX_MEDIA_SERVER_USE_SYSLOG=true # send all plex logs to syslog
        #      - PLEX_CLAIM_TOKEN=
    networks:
      - app_net
      - discovery
    ports:
      - "32400:32400"
      - "32400:32400/udp"
      - "32469:32469"
      - "32469:32469/udp"
      - "5353:5353/udp"
      - "1900:1900/udp"
    expose:
      - "32400"
    labels:
      - "traefik.port=32400"
      - "traefik.frontend.rule=Host:${PLEX_VHOST}"
    volumes:
      - ${PLEX_DIR}/db:/config # plex database
      - ${PLEX_DIR}/transcode:/transcode # temp transcoded files
      - ${DATA_DIR}/downloads:/data # media library
      - ${BACKUP_DIR}/plex:/backups
    logging:
      driver: gelf
      options:
        gelf-address: "udp://${GELF_ADDRESS}:${GELF_PORT}"
        tag: "plex"
    
  tautulli:
    container_name: tautulli
    image: linuxserver/tautulli:${TAUTULLI_TAG}
    restart: always
    depends_on:
      - plex
    networks:
      - app_net
      - discovery
    environment:
      - PUID=${PUID} # required for proper nfs access
      - PGID=${PGID} # required for proper nfs access
      - TZ=America/Los_Angeles
    labels:
      - "traefik.frontend.rule=Host:${TAUTULLI_VHOST}"
    volumes:
      - plex_logs:/logs:ro
      - ${CONF_DIR}/.config/tautulli:/config
    logging:
      driver: gelf
      options:
        gelf-address: "udp://${GELF_ADDRESS}:${GELF_PORT}"
        tag: "tautulli"

volumes:
  plex_logs:
    driver: local
    driver_opts:
      device: "${PLEX_DIR}/db/Library/Application Support/Plex Media Server/Logs"
      o: bind
      type: none
